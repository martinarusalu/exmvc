(function () {
    'use strict';

    var messages = {
        'NotNull.order.orderNumber' : 'Order number is missing',
        'Size.order.orderNumber' : 'Order number must be more than {1} characters',
    };

    angular.module('app').service('messageService', Srv);

    function Srv() {

        this.getMessage = getMessage;

        function getMessage(key, params) {
            var text = messages[key];

            if (text === undefined) {
                return "Unknown message key: " + key;
            }

            if (params === undefined) {
                return text;
            }

            for (var i = 0; i < params.length; i++) {
                text = text.replace(new RegExp('\\{' + (i + 1) + '\\}', 'g'), params[i]);
            }

            return text;
        }

    }

})();
