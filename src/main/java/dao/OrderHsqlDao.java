package dao;

import model.Order;
import model.OrderRow;
import model.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import javax.validation.Valid;
import java.sql.PreparedStatement;
import java.util.List;

@Primary
@Repository
public class OrderHsqlDao implements OrderDao {

    @Autowired
    private OrderRowDao rowDao;

    @Autowired
    private JdbcTemplate template;

    @Override
    public Order save(Order order) {
        String sql = "insert into orders (order_number) "
                   + "values (?)";

        GeneratedKeyHolder holder = new GeneratedKeyHolder();

        template.update(conn -> {
            PreparedStatement ps = conn.prepareStatement(sql, new String[] {"id"});
            ps.setString(1, order.getOrderNumber());

            return ps;
        }, holder);

        order.setId(holder.getKey().longValue());

        if (order.getOrderRows() != null) {
            for (OrderRow row : order.getOrderRows()) {
                row.setOrderId(order.getId());
                rowDao.save(row);
            }
        }

        return order;
    }

    @Override
    public List<Order> findAll() {
        String sql = "select id, order_number from orders";

        List<Order> orders = template.query(sql, (rs, rowNum) -> {
            return new Order(
                    rs.getString("order_number"),
                    rs.getLong("id"));
        });

        for (Order o : orders) {
            List<OrderRow> rows = rowDao.findByOrderId(o.getId());
            System.out.println(rows);
            o.addMultiple(rows);
        }

        return orders;
    }

    @Override
    public void delete(Long id) {
        String sql = "delete from orders where id = ?";

        template.update(sql, id);
    }

    @Override
    public void deleteAll() {
        String sql = "delete from orders";

        template.update(sql);
    }

    @Override
    public Order findById(Long id) {
        String sql = "select id, order_number "
                   + "from orders where id = ?";

        Order order = template.queryForObject(sql, new Object[] {id}, (rs, rowNum) -> {
            return new Order(
                    rs.getString("order_number"),
                    rs.getLong("id"));
            });

        order.addMultiple(rowDao.findByOrderId(order.getId()));
        return order;
    }

    @Override
    public Report getReport() {
        String sql = "SELECT count(orders.id), " +
                "(SELECT AVG(\"orderPrice\") from (" +
                "SELECT SUM(order_rows.price * order_rows.quantity) as \"orderPrice\" from order_rows GROUP BY order_id" +
                ") t1) as \"averageOrderAmount\", " +
                "(SELECT SUM(order_rows.price * order_rows.quantity) from order_rows) as \"turnoverWithoutVAT\", " +
                "(SELECT SUM(order_rows.price * order_rows.quantity) / 5 from order_rows) as \"turnoverVAT\"," +
                "(SELECT SUM(order_rows.price * order_rows.quantity) + (SUM(order_rows.price * order_rows.quantity) / 5) from order_rows) as \"turnoverWithVAT\" from orders;";

        Report report = template.query(sql, (rs, rowNum) -> {
            return new Report(rs.getInt(1),
                    rs.getInt(2),
                    rs.getInt(3),
                    rs.getInt(4),
                    rs.getInt(5));
        }).get(0);

        return report;
    }

}
