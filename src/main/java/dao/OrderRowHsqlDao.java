package dao;

import model.OrderRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.List;

@Primary
@Repository
public class OrderRowHsqlDao implements OrderRowDao {

    @Autowired
    private JdbcTemplate template;

    @Override
    public OrderRow save(OrderRow orderRow) {
        String sql = "insert into order_rows (item_name, quantity, price, order_id) "
                   + "values (?, ?, ?, ?)";

        template.update(conn -> {
            PreparedStatement ps = conn.prepareStatement(sql, new String[] {"id"});
            ps.setString(1, orderRow.getItemName());
            ps.setInt(2, orderRow.getQuantity());
            ps.setInt(3, orderRow.getPrice());
            ps.setLong(4, orderRow.getOrderId());

            return ps;
        });

        return orderRow;
    }

    @Override
    public List<OrderRow> findByOrderId(Long orderId) {
        String sql = "select item_name, quantity, price, order_id from order_rows where order_id = ?";

        return template.query(sql, (rs, rowNum) -> {
            return new OrderRow(
                    rs.getString("item_name"),
                    rs.getInt("quantity"),
                    rs.getInt("price"),
                    rs.getLong("order_id"));
        }, new Object[] {orderId});
    }

    @Override
    public void delete(Long id) {
        String sql = "delete from order_rows where id = ?";

        template.update(sql, id);
    }

}
