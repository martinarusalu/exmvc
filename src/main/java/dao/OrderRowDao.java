package dao;

import model.OrderRow;

import java.util.List;

public interface OrderRowDao {

    OrderRow save(OrderRow orderRow);

    void delete(Long id);

    List<OrderRow> findByOrderId(Long id);
}
