package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Report {
    public Integer count;
    public Integer averageOrderAmount;
    public Integer turnoverWithoutVAT;
    public Integer turnoverVAT;
    public Integer turnoverWithVAT;
}
