package controller;

import dao.OrderDao;
import model.Order;
import model.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

@RestController
public class OrderController {

    @Autowired
    private OrderDao dao;

    @GetMapping("orders")
    public List<Order> getOrders() {
        return dao.findAll();
    }

    @GetMapping("orders/{orderId}")
    public Order getOrderById(@PathVariable("orderId") Long orderId) {
        return dao.findById(orderId);
    }

    @GetMapping("orders/report")
    public Report getReport() {
        return dao.getReport();
    }

    @PostMapping("orders")
    public Order save(@RequestBody @Valid Order order) {
        return dao.save(order);
    }

    @DeleteMapping("orders/{orderId}")
    public void delete(@PathVariable("orderId") Long orderId) {
        dao.delete(orderId);
    }

    @DeleteMapping("orders")
    public void deleteAll() {
        dao.deleteAll();
    }
}